import React from 'react';
import s from './PickSong.module.scss';

import MainLayout from 'layouts/Main';

import pickSongTopper from 'assets/images/pickSongTopper.svg';
import wink from 'assets/images/wink.svg';
import Footer from 'assets/images/footer.svg';

import { useGlobalState } from 'store/globalState';
import appState, { allSongs } from 'store/index';
import { useHistory } from 'react-router-dom';

const PickSong = ({children}) => {
  const [chosenSong, dispatch] = useGlobalState(appState, (state) => state.chosenSong);
  const history = useHistory();

  console.log('reducer stuffs??? ', chosenSong);
  console.log('songs?? ', allSongs);

  const pickingSong = (id) => {
    dispatch(id); 
    history.push('/gallery')
  }

  return (
    <MainLayout>
      <div className={s.pickSong}>
        <img className={s.image} src={pickSongTopper} alt="musical topper" />
        <div>
          <h1 className={s.title}>Choose your carol</h1>
          <div className={s.secondLine}>
            <p className={s.text}>Performed Live By Folks From Our Talented Team</p>
            <img className={s.wink} src={wink} alt="winking emoji" />
          </div>
        </div>
        <div className={s.buttons}>
          { allSongs.map((song, i) => {
            return (
              <button className={s.button} onClick={() => pickingSong(song.id)}>{ song.name }</button>
            )
          })}
        </div>
      <img className={s.footer} src={Footer} alt="Happy Holidays from 14Four" />
      </div>
    </MainLayout>
  )
}

export default PickSong;