import React from 'react'
import s from './Home.module.scss'

import MainLayout from 'layouts/Main'
import HomeHero from 'components/HomeHero'

const Home = ({children}) => {
  return (
    <MainLayout>
      <div className={s.Home}>
        <HomeHero />
      </div>
    </MainLayout>
  )

}

export default Home
