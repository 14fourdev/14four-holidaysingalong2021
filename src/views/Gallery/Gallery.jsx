import React, {useEffect, useState} from 'react'
import s from './Gallery.module.scss'

import ExperienceLoader from 'scripts/loaders/ExperienceLoader'
import ARLoader from 'scripts/loaders/ARLoader'
import EventBus from 'scripts/util/EventBus';

import MainLayout from 'layouts/Main'
import Loading from 'components/Loading/Loading';
import AfterSong from 'components/AfterSong/AfterSong';
import appState from 'store/index';
import Overlay from 'components/Overlay/Overlay';
const Gallery = ({children}) => {

  const [loading, setLoading] = useState(true);
  const [hasEnded, setHasEnded] = useState(false);
  const [showText, setShowText] = useState(true);

  useEffect(() => {
    console.log('in use effect');
    EventBus.on('Action', handleAction)
    ARLoader.load().then(() => {
      ExperienceLoader.load("Main").then(onLoadModule)     
    })
  }, []) // eslint-disable-line

  const onLoadModule = (module) => {
    const experience = new module.default()
    const canvas = document.getElementById('ar-canvas')
    experience.initPipeline(canvas)
    experience.load().then(() => {
      // const chosenSong = appState.state.chosenSong.id;
      const chosenSong = JSON.parse(localStorage.getItem('chosenSong'));
      // alert(chosenSong.chosenSong.id);
      // console.log("CHOSEN SONG:");
      // console.log(chosenSong);
      experience.initExperience(chosenSong.chosenSong.id);
    })
  }

  const handleAction = (payload) => {
    console.log('Event:', payload)
    if(payload.type === 'loadingDone') {
      setLoading(false);
    }
    if(payload.type === 'singersPlaced') {
      setShowText(false);
    }
    if(payload.type === 'songOver') {
      setHasEnded(true);
    }
  }

  return (
    <MainLayout>
      <div className={s.Gallery}>
        <Overlay show={!loading && !hasEnded} displayText={showText} />
        <AfterSong show={hasEnded} />
        <Loading show={loading} />
      </div>
    </MainLayout>
  )

}

export default Gallery
