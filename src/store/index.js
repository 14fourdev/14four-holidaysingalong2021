import { GlobalState } from "./globalState"

export const allSongs = [
  {
    id: 'JBR',
    name: 'Jingle Bell Rock',
    carolers: ['Ryan Babcock', 'Laura Kneeshaw', 'Simeon Smith', 'Jessica Borda']
  },
  {
    id: 'RAXT',
    name: 'Rockin Around the xmas tree',
    carolers: ['Travis Nelson', 'Patrick Nance', 'Rich Lane', 'Jeff Oswalt']
  },
  {
    id: 'JB911',
    name: 'Jingle Bells 911',
    carolers: ['Brandon Delauney', 'Devin Taylor', 'Madison Merica', 'Steve Wanless']
  },
  {
    id: 'WWYMX',
    name: 'We Wish you a merry christmas',
    carolers: ['James Clark', 'Murphy Mumm', 'Avery McKee', 'Peter Dunau', 'Eric Lewis']
  },
  {
    id: 'LIS',
    name: 'let it snow',
    carolers: ['Jared Darr', 'Alex Michaluk', 'Jesse Ward', 'Coby Forrester', 'RJ Miller']
  },
]

const reducer = (state, action) => {
  switch(action) {
    case 'JBR':
      return {
        ...state,
        chosenSong: allSongs[0]
      }
    case 'RAXT':
      return {
        ...state,
        chosenSong: allSongs[1]
      }
    case 'JB911':
      return {
        ...state,
        chosenSong: allSongs[2]
      }
    case 'WWYMX':
      return {
        ...state,
        chosenSong: allSongs[3]
      }
    case 'LIS':
      return {
        ...state,
        chosenSong: allSongs[4]
      }
    default:
      console.log('its done borked');
      break;
  }
}

const appState = new GlobalState(reducer, { chosenSong: JSON.parse(localStorage.getItem('chosenSong')) });

export default appState;