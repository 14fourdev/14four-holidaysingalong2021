import React from 'react';
import s from './AfterSong.module.scss';
import Smiles from 'assets/images/smiles.svg';
import Mistletoe from 'assets/images/mistletoe.svg';
import Footer from 'assets/images/footer.svg';

export default function AfterSong(props) {
  if(!props.show) {
    return null;
  }
  return (
    <div className={s.afterSong}>
      <img className={s.topper} src={Smiles} alt="Smiling Emoji" />
      <h1 className={s.title}>
        <span>Sure, you could leave now,</span>
        <span>all full of holiday cheer</span>
        <span className={s.line}>or</span>
        <span className={s.diff}>try another?</span>
      </h1>
      <p className={s.text}>There’s more talent and more songs waiting to be enjoyed!</p>
      <div className={s.buttonRow}>
        <img className={s.leftImg} src={Mistletoe} alt="Mistletoe" />
        <a href="/PickSong" className={s.button}>more please</a>
        <img className={s.rightImg} src={Mistletoe} alt="Mistletoe" />
      </div>
      <img className={s.footer} src={Footer} alt="Happy Holidays from 14Four" />
    </div>
  )
}