import React, { useEffect, useState } from 'react';
import s from './Loading.module.scss';
import Topper from 'assets/images/pickSongTopper.svg';
import Wall from 'assets/images/8thWall.svg';
import MusicNote from 'assets/images/MusicNote.svg';
import DarkNote from 'assets/images/DarkMusicNote.svg';

import appState from 'store/index';

export default function Loading(props) {
  // const singers = appState.state.chosenSong.carolers;
  const singers = JSON.parse(localStorage.getItem('chosenSong')).chosenSong.carolers;
  const [allSingersDisplayed, setAllSingersDisplayed] = useState(false);

  const singersLoadingIn = (
    <div className={s.line}>
      <img className={s.musicNote} src={DarkNote} alt="Musical Note" />
      <div className={s.dots}>
        <div className={s.dot1}></div>
        <div className={s.dot2}></div>
        <div className={s.dot3}></div>
      </div>
    </div>
  );
  const [count, setCount] = useState(0);

  useEffect(() => {
    let counter = count;
    const interval = setInterval(() => {
      // if(props.show && counter === singers.length -2) {
      //   console.log('Are you the last boi???')
      // }
      if(counter >= singers.length){
        console.log('in last if', props.show)
        clearInterval(interval);
        setAllSingersDisplayed(true);
      } else if(!props.show || counter !== singers.length-1) {
        setCount(count => count+1);
        counter++;
      }
    }, 2000);
    return () => clearInterval(interval);
  }, [singers]);

  let slowList = singers.slice(0, count).map((singer, index) => {
    return (
      <div className={s.line} key={index}>
        <img className={s.musicNote} src={MusicNote} alt="Musical Note" />
        <p className={s.singer}>{singer}</p>
      </div>
    )
  });

  const id = JSON.parse(localStorage.getItem('chosenSong')).chosenSong.id;
  // Don't judge me for this... 
  // Not sure how to dynamically add class names 
  // with this react dot notation or whatever
  const minWidth = {
    minWidth: id === 'JBR' ? '185px' : 
              id === 'RAXT' ? '164px' :
              id === 'JB911' ? '203px' :
              id === 'WWYMX' ? '170px' :
              id === 'LIS' ? '173px' : '150px'
  }
  if(!props.show && allSingersDisplayed) {
    return null;
  }
  return (
    <div className={s.loading}>
      <img className={s.img} src={Topper} alt="Decoration" />
      <div>
        <h1 className={s.title}>Waiting for the Carolers<br />to join the Party</h1>
        <p className={s.subText}>( be sure your sound is on )</p>
        <hr className={s.divider} />
        <div className={s.singers} style={minWidth}>
          {slowList}
          {count !== singers.length && singersLoadingIn}
        </div>
      </div>
      <div className={s.padding}></div>
      {/* <img className={s.wall} src={Wall} alt="Powered by 8th Wall" /> */}
    </div>
  )
}