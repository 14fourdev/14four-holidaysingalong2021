import React from 'react';
import s from './Desktop.module.scss';

import Topper from 'assets/images/desktop-topper.svg';
import Mistletoe from 'assets/images/mistletoe.svg';
import QRCode from 'assets/images/qr-code.png';
import Footer from 'assets/images/desktop-footer.svg';

export default function DesktopPage(props) {
  return (
    <div className={s.desktop}>
      <img className={s.topper} src={Topper} alt="Decoration" />
      <div>
        <p className={s.bigText}>TELEPRESENCE CAROLING</p>
        <p className={s.littleText}>IS A MOBILE-ONLY EXPERIENCE. SCAN THE QR CODE BELOW AND MAKE YOUR YULE-TIDE GAY!</p>
        <div className={s.rowOfImages}>
          <img className={s.leftImg} src={Mistletoe} alt="Mistletoe" />
          <img className={s.qrCode} src={QRCode} alt="QRCode to falala.14four.com" />
          <img className={s.rightImg} src={Mistletoe} alt="Mistletoe" />
        </div>
      </div>
      <img className={s.footer} src={Footer} alt="Happy Holidays from 14Four" />
    </div>
  )
}
