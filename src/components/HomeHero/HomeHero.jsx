import React from 'react'
import s from './HomeHero.module.scss'
import HomeTopper from 'assets/images/homeTopper.svg';
import Mistletoe from 'assets/images/mistletoe.svg';
import Footer from 'assets/images/footer.svg';

export default function HomeHero(props) {
  return (
    <div className={s.home}>      
      <img className={s.img} src={HomeTopper} alt="Decoration" />
      <h1 className={s.title}>
        <span>Congratulations</span>
        <span>You've unlocked the magic</span>
        <span className={s.line}>of</span>
        <span className={s.diff}>telepresence</span>
        <span className={s.diff}>caroling</span>
      </h1>
      <p className={s.text}>( The Fun And Safe Way To Spread Holiday Cheer )</p>
      <div className={s.buttonRow}>
        <img className={s.leftImg} src={Mistletoe} alt="MistleToe" />
        <a href="/PickSong" className={s.button}>Start</a>
        <img className={s.rightImg} src={Mistletoe} alt="MistleToe" />
      </div>
      <img className={s.footer} src={Footer} alt="Happy Holidays from 14Four" />
    </div>
  );
}
