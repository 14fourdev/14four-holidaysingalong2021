
import React from 'react'
import { useWindowSize } from 'hooks/useWindowSize'
import Div100vh from 'react-div-100vh'
 
const MAX_WIDTH = 980
const VerticleHeight = ({ children, className }) => {
  const customClass = className || ''
  const { width } = useWindowSize();

  if(width < MAX_WIDTH){
    return <Div100vh className={customClass}>
      {children}
    </Div100vh>
  } else {
    return <>
      {children}
    </>
  }

}

export default VerticleHeight
