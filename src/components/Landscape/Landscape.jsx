import React from 'react';
import s from './Landscape.module.scss';
import rotate from 'assets/images/rotate.svg';

export default function Landscape(props) {
  return (
    <div className={s.page}>
      <img className={s.image} src={rotate} alt="Rotate Phone"/>
      <h1 className={s.title}>Rotate your device</h1>
      <p className={s.text}>This AR experience only works in portrait mode.</p>
    </div>
  )
}