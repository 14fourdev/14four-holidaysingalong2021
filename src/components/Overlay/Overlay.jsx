import React from 'react';
import s from './Overlay.module.scss';

import Mistletoe from 'assets/images/mistletoe.svg';
import Close from 'assets/images/close.svg';

export default function Overlay(props) {

  if(!props.show) {
    return null;
  }
  return (
    <div className={s.overlay}>
      { props.displayText && 
        <div className={s.row}>
          <img className={s.leftImg} src={Mistletoe} alt="Mistletoe" />
          <p className={s.TapToStart}>Tap to place<br />Pinch to scale</p>
          <img className={s.rightImg} src={Mistletoe} alt="Mistletoe" />
        </div>
      }
      <a href="/PickSong" className={s.close}>
        <img src={Close} alt="Back to Pick Song" />
      </a>
    </div>
  );
}