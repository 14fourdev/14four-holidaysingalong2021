import React from 'react'
import Home from 'views/Home'
import PickSong from 'views/PickSong'
import Gallery from 'views/Gallery'
import NotFound from 'views/NotFound'

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/PickSong">
            <PickSong />
          </Route>
          <Route exact path="/gallery">
            <Gallery />
          </Route>
          <Route path="/*">
            <NotFound />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
