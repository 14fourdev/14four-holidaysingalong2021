import AssetLoader from "scripts/loaders/ThreejsAssetLoader";
import ARPipeline from "scripts/pipeline/ThreeARPipeline";
import * as THREE from "three";
import TouchHandler from "scripts/util/TouchHandler";
import EventBus from "scripts/util/EventBus";

export default class MainExperience {
  initPipeline = (canvas) => {
    this.running = false;
    this.placed = false;
    this.pinched = false;
    this.canvas = canvas;
    this.video = null;

    // in seconds
    this.videoTimeins = [
      10, //0 jingle bell rock
      10.5, //1 rock around christmas tree
      7.5, //2 jingle bells 911
      9, //3 we wish you a merry xmas
      11.5, //4 let it snow
    ];
    this.videoTimeouts = [
      128, //0 jingle bell rock
      124.5, //1 rock around christmas tree
      39, //2 jingle bells 911
      103, //3 we wish you a merry xmas
      111, //4 let it snow
    ];

    //animation
    this.clock = new THREE.Clock();
    this.mixer = null;
    this.actions = null;

    ARPipeline.setExperience(this);
    THREE.Cache.enabled = true;
  };

  load = () => {
    return new Promise((resolve) => {
      AssetLoader.load({
        models: {
          robo: "/models/Robot/Robot_v3.gltf",
        },
        textures: {
          // environment: "/lighting/Environment.hdr",
          greenColor: "/models/Robot/Textures/RobotBody_G_BaseColor.jpg",
          greenEmissive: "/models/Robot/Textures/RobotBody_G_Emissive.jpg",
          // crosshairs: "/textures/face.png",
        },
        hdris: {
          snow: "/lighting/Environment.hdr",
        },
        // sounds: {
        //   test: "/sfx/test.mp3",
        // },
      }).then(() => {
        // alert("loaded");
        EventBus.dispatch("Action", {
          type: "loadingDone",
        });
        document.getElementById("ar-canvas").style.zIndex = "0";
        resolve();
      });
    });
  };

  initExperience = (chosenSong) => {
    this.VIDEONUM = 0;
    if (chosenSong === "JBR") this.VIDEONUM = 0;
    if (chosenSong === "RAXT") this.VIDEONUM = 1;
    if (chosenSong === "JB911") this.VIDEONUM = 2;
    if (chosenSong === "WWYMX") this.VIDEONUM = 3;
    if (chosenSong === "LIS") this.VIDEONUM = 4;
    this.initCamera();
    this.initAudio();
    this.initRenderer();
    this.initScene();
    this.initLights();
    this.initTouchHandler();
    this.running = true;
  };

  initCamera = () => {
    this.camera = ARPipeline.camera;
  };

  initAudio = () => {
    const listener = new THREE.AudioListener();
    this.camera.add(listener);

    const sound = new THREE.Audio(listener);
    this.sfx = sound;
    const audioLoader = new THREE.AudioLoader();
    audioLoader.load("/sfx/test.mp3", function (buffer) {
      sound.setBuffer(buffer);
      sound.setLoop(true);
      sound.setVolume(1);
    });

    // const sound = new THREE.Audio(listener);
    // this.sfx = sound;
    // let buffer = AssetLoader.getSound("test");
    // this.sfx.setBuffer(buffer);
    // this.sfx.setVolume(1);
    // console.log(this.sfx);

    // this.sfx.play();
    // const audioAnalyser = new THREE.AudioAnalyser( sound, 32 );
  };

  initRenderer = () => {
    this.renderer = ARPipeline.renderer;
    this.renderer.physicallyCorrectLights = true;
    this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
    this.renderer.toneMappingExposure = 1.1;
    this.renderer.antialiasing = true;
    this.renderer.outputEncoding = THREE.sRGBEncoding;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMapSoft = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    this.renderer.context.canvas.addEventListener(
      "webglcontextlost",
      function (event) {
        event.preventDefault();
        // animationID would have been set by your call to requestAnimationFrame
        // cancelAnimationFrame(animationID);
        console.log("listener: context lost");
      },
      false
    );

    this.renderer.context.canvas.addEventListener(
      "webglcontextrestored",
      function (event) {
        // Do something
        console.log("listener: context restored");
      },
      false
    );
  };

  songOver = () => {
    for (let i = 0; i < this.NUM_OF_FACES; i++)
    {
      setTimeout(() => {
        this.actionIntro[i].reset().fadeIn(1).play();
        this.actionDance[i].fadeOut(1);
      }, 1000); 
      setTimeout(() => {
        this.actionOff[i].reset().fadeIn(1).play();
        this.actionIntro[i].fadeOut(1);
      }, 4500); 
    }
    setTimeout(() => {
      EventBus.dispatch("Action", {
        type: "songOver",
      });
      document.getElementById("ar-canvas").style.zIndex = "-1";
    }, 5500);
  };

  initScene = () => {
    this.scene = ARPipeline.scene;

    const environment = AssetLoader.getHDRI("snow");
    environment.mapping = THREE.EquirectangularReflectionMapping;
    this.environmentMap = environment;
    this.scene.environment = environment;

    //view acts as container for everything
    const view = new THREE.Group();
    this.view = view;
    // this.view.scale.set(.7,.7,.7);
    //specify number of faces per song
    //0 jingle bell rock
    //1 rock around christmas tree
    //2 jingle bells 911
    //3 we wish you a merry xmas
    //4 let it snow
    this.NUM_OF_FACES = 4;
    if (this.VIDEONUM >= 3) this.NUM_OF_FACES = 5;

    //Get your video element:
    this.video = document.getElementById("video" + this.VIDEONUM);
    //this.video.preload = "auto";

    // this.video.addEventListener("ended", videoEnded.bind(this), false);
    // function videoEnded(e) {
    //   this.songOver();
    // }

    //separate out video faces - randomize hats------------
    // needs mp4 with AAC audio
    let faces = [];
    this.faces = faces;
    const videoTexture = new THREE.VideoTexture(this.video);
    videoTexture.magFilter = THREE.RGBAFormat;
    // videoTexture.flipY=false;
    let videoTextureMaterial = new THREE.MeshBasicMaterial({
      map: videoTexture,
      side: THREE.FrontSide,
      toneMapped: true,
    });

    for (let i = 0; i < this.NUM_OF_FACES; i++) {
      var geometry = new THREE.PlaneGeometry(2.34, 2.34);
      var uv = geometry.getAttribute("uv");
      if (i === 0) {
        uv.setXY(0, 0, 1);
        uv.setXY(1, 0.333, 1);
        uv.setXY(2, 0, 0.5);
        uv.setXY(3, 0.333, 0.5);
      }
      if (i === 1) {
        uv.setXY(0, 0.333, 1);
        uv.setXY(1, 0.666, 1);
        uv.setXY(2, 0.333, 0.5);
        uv.setXY(3, 0.666, 0.5);
      }
      if (i === 2) {
        uv.setXY(0, 0.666, 1);
        uv.setXY(1, 1, 1);
        uv.setXY(2, 0.666, 0.5);
        uv.setXY(3, 1, 0.5);
      }
      if (i === 3) {
        uv.setXY(0, 0, 0.5);
        uv.setXY(1, 0.333, 0.5);
        uv.setXY(2, 0, 0);
        uv.setXY(3, 0.333, 0);
      }
      if (i === 4) {
        uv.setXY(0, 0.333, 0.5);
        uv.setXY(1, 0.666, 0.5);
        uv.setXY(2, 0.333, 0);
        uv.setXY(3, 0.666, 0);
      }
      uv.needsUpdate = true;
      var mesh = new THREE.Mesh(geometry, videoTextureMaterial);
      mesh.position.set(0, 0, -0.15);
      faces[i] = mesh;
    }
    let hats = [];
    let currentHat = Math.floor(Math.random() * 3);
    for (let i = 0; i < this.NUM_OF_FACES; i++) {
      currentHat++;
      if (currentHat >= 3) currentHat = 0;
      hats[i] = currentHat;
    }
    faces.sort(() => (Math.random() > 0.5 ? 1 : -1));

    //glass
    const materialGlass = new THREE.MeshPhysicalMaterial({
      roughness: 0, //0.05,
      transmission: 1,
      thickness: 0.05, //.3, // refract
      color: new THREE.Color(0x444444),
    });
    materialGlass.environmentMap = this.environmentMap;
    this.mixer = [];
    this.robo = [];
    this.actionOff = [];
    this.actionIntro = [];
    this.actionDance = [];
    this.robo[0] = AssetLoader.getModel("robo");
    this.clips = this.robo[0].animations;
    for (let i = 0; i < this.clips.length; i++) console.log(this.clips[i]);

    //first creat additional robos:
    for (let i = 1; i < this.NUM_OF_FACES; i++) {
      this.robo[i] = this.cloneGltf(this.robo[0]);
      this.robo[i] = this.robo[i].scene;
      this.robo[i].traverse(function (child) {
        if (child.material) child.material = child.material.clone();
      });
    }
    this.robo[0] = this.robo[0].scene;

    let clipOff = THREE.AnimationClip.findByName(this.clips, "RobotOff");
    let clipIntro = THREE.AnimationClip.findByName(this.clips, "Idle");
    let clipDance = null;
    //0 jingle bell rock
    //1 rock around xmas tree
    //2 jingle bells 911
    //3 we wish you a merry xmas
    //4 let it snow
    if (this.VIDEONUM === 0)
      clipDance = THREE.AnimationClip.findByName(this.clips, "Loop_JBR");
    if (this.VIDEONUM === 1)
      clipDance = THREE.AnimationClip.findByName(this.clips, "Loop_JBR");
    if (this.VIDEONUM === 2)
      clipDance = THREE.AnimationClip.findByName(this.clips, "Loop_JB911");
    if (this.VIDEONUM === 3)
      clipDance = THREE.AnimationClip.findByName(this.clips, "Loop_WeWish");
    if (this.VIDEONUM === 4)
      clipDance = THREE.AnimationClip.findByName(this.clips, "Loop_LetItSnow"); //LetItSnow
    clipOff.optimize();
    clipIntro.optimize();
    clipDance.optimize();

    let angInc = Math.PI / 6;
    let ang = 0 - Math.PI / 2 - angInc;
    if (this.NUM_OF_FACES === 4) ang -= angInc / 2;
    let rad = 1.5;
    //now set up each robo
    for (let i = 0; i < this.NUM_OF_FACES; i++) {
      this.mixer[i] = new THREE.AnimationMixer(this.robo[i]);
      this.actionOff[i] = this.mixer[i].clipAction(clipOff);
      this.actionOff[i].repetitions = 1000;
      this.actionIntro[i] = this.mixer[i].clipAction(clipIntro);
      this.actionIntro[i].repetitions = 1000;
      // this.actionIntro[i].timeScale = 1;
      this.actionDance[i] = this.mixer[i].clipAction(clipDance);
      this.actionDance[i].repetitions = 1000;

      this.actionOff[i].play();
      // this.actionOff[i].paused = true;
      //proper fade out - will move this to when placed is true
      // setTimeout(() => {
      //   // alert("timeout");
      //   this.actionDance[i].reset().fadeIn(3).play();
      //   this.actionIntro[i].fadeOut(3);
      // }, 3000);
      // will use this to fire off any single fire anims
      // mixer.addEventListener("finished", function (e) {
      //   actions[1].play();
      //   actions[0].crossFadeTo(actions[1], 0.5);
      // });

      this.robo[i].traverse(function (child) {
        //Antlers, HatGreen, HatRed, Head_Glass_low, Screen_low, Body_low_Cylinder023
        if (child instanceof THREE.Mesh) {
          if (child.name === "Screen_low") {
            //child.scale.set(0,0,0);
            //child.material = faces[i]; //Math.floor(Math.random() * 4) + 1];
            child.add(faces[i]);
            child.material.transparent = true;
          }
          if (
            hats[i] === 0 &&
            (child.name === "Antlers" || child.name === "HatGreen")
          )
            child.scale.set(0, 0, 0);
          if (
            hats[i] === 1 &&
            (child.name === "HatRed" || child.name === "HatGreen")
          )
            child.scale.set(0, 0, 0);
          if (
            hats[i] === 2 &&
            (child.name === "Antlers" || child.name === "HatRed")
          )
            child.scale.set(0, 0, 0);

          if (child.name === "Head_Glass_low") {
            child.material = materialGlass;
            // child.material.opacity = 0.3;
            // child.scale.set(0, 0, 0);
          }

          //body colors
          if (child.name === "Body_low_Cylinder023" && i % 2 === 0) {
            console.log("body:");
            console.log(child.material);
            let mat = AssetLoader.getTexture("greenColor");
            mat.flipY = false; //gltf loader flips y
            child.material.map = mat;
            mat = AssetLoader.getTexture("greenEmissive");
            mat.flipY = false;
            child.material.emissiveMap = mat;
          }
        }
      });

      this.robo[i].scale.set(0.1, 0.1, 0.1);

      // const geometry = new THREE.SphereGeometry(0.5, 32, 16);
      // const material = new THREE.MeshBasicMaterial({ color: 0xff0000 });
      // const sphere = new THREE.Mesh(geometry, material);
      // sphere.position.set(rad * Math.sin(ang), -.1, rad * Math.cos(ang));
      // sphere.scale.set(1,0,1);
      // view.add(sphere);
      this.robo[i].position.set(
        rad * Math.sin(ang),
        0,
        rad * Math.cos(ang) + rad
      );
      ang -= angInc;
      this.robo[i].rotation.y = Math.atan2(
        -this.robo[i].position.x,
        5 - this.robo[i].position.z
      );
      view.add(this.robo[i]);
    }

    view.position.set(0, 0, 0);
    this.scene.add(view);
    this.showView();

    //floor surface----------
    const surfaceMaterial = new THREE.ShadowMaterial({
      opacity: 0.25,
      depthWrite: false,
    });
    this.surface = new THREE.Mesh(
      new THREE.PlaneGeometry(100, 100, 1, 1),
      surfaceMaterial
    );
    this.surface.rotateX(-Math.PI / 2);
    this.surface.position.set(0, 0, 0);
    this.surface.receiveShadow = true;
    this.scene.add(this.surface);
    this.centerPoint = new THREE.Vector2(0, 0);
    this.lookAtMe = new THREE.Vector3(0, 0, 0);
    this.raycaster = new THREE.Raycaster();
    // const crossMaterial = new THREE.MeshBasicMaterial({});
    // crossMaterial.map = AssetLoader.getTexture("crosshairs");
    // crossMaterial.transparent = true;
    // this.crosshairs = new THREE.Mesh(
    //   new THREE.PlaneGeometry(1, 1, 1, 1),
    //   crossMaterial
    // );
    // this.crosshairs.rotateX(-Math.PI / 2);
    // this.scene.add(this.crosshairs);
  };

  showView = (show = true) => {
    this.view.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.visible = show;
        child.castShadow = true;
      }
    });
  };

  initLights = () => {
    this.ambien = new THREE.AmbientLight(0x122130, 2); //(0xFFFFFF, 0.7);
    this.scene.add(this.ambien);

    this.light = new THREE.DirectionalLight(0xffffff, 0.5);
    this.light.castShadow = true;
    this.view.add(this.light);
    this.light.position.set(-30, 100, 30);
    this.light.shadow.radius = 12.9;
    this.light.shadow.mapSize.width = 2048;
    this.light.shadow.mapSize.height = 2048;
    this.light.target = this.view;
  };

  // ------------------ TOUCHY STUFF
  initTouchHandler() {
    const minScale = 0.1;
    const maxScale = 20;
    const scaleMultiplier = 5;
    TouchHandler.init(
      this.scene,
      this.camera,
      this.surface,
      this.view,
      minScale,
      maxScale,
      scaleMultiplier
    );
    this.canvas.addEventListener(
      "touchstart",
      (e) => {
        e.preventDefault();
        this.handleCanvasTouch(e, "start");
      },
      { passive: true }
    );
    this.canvas.addEventListener(
      "touchmove",
      (e) => {
        e.preventDefault();
        this.handleCanvasTouch(e, "move");
      },
      { passive: true }
    );
    this.canvas.addEventListener(
      "touchend",
      (e) => {
        e.preventDefault();
        this.handleCanvasTouch(e, "end");
      },
      { passive: true }
    );
  }

  handleCanvasTouch(e, state) {
    if (state === "end") {
      TouchHandler.endGesture();
      return;
    }
    if (e.touches.length > 2) return;
    if (e.touches.length === 1) {
      if (this.placed === false) {
        // this.sfx.play();
        // this.sfx.setVolume(0);
        // this.sfx.pause();
        // for (let i = 0; i < this.NUM_OF_FACES; i++)
        //   if (this.actionIntro[i]) this.actionIntro[i].paused = false;
        this.video.play();
        //this.placeCarolers();
        this.video.addEventListener("playing", videoStarted.bind(this), false);
        function videoStarted(e) {
          this.placeCarolers();
        }

        this.placed = true;
        // this.crosshairs.scale.set(0, 0, 0);
        EventBus.dispatch("Action", {
          type: "singersPlaced",
        });
        TouchHandler.handleCanvasTouchPlace(
          e,
          state,
          true,
          false,
          this.view.position
        );
      } else TouchHandler.handleCanvasTouchPlace(e, state);
      this.showView();
    }
    if (e.touches.length === 2) TouchHandler.handleCanvasTouchScale(e, state);
  }
  // -------END TOUCHY STUFF

  // prepChildren = (children) => {
  //   let child;
  //   for (let i = 0; i < children.length; i++) {
  //     child = children[i];

  //     if (child.material) {
  //       child.castShadow = true;
  //       //child.receiveShadow = true;
  //       //set up material for hdri
  //       child.material.envMap = this.environmentMap;
  //       child.material.needsUpdate = true;
  //     }
  //     //no need for recursion on this model
  //     if (child.children) this.prepChildren(child.children);
  //   }
  // };

  placeCarolers() {
    for (let i = 0; i < this.NUM_OF_FACES; i++) {
      setTimeout(() => {
        this.actionIntro[i].reset().fadeIn(1).play();
        this.actionOff[i].fadeOut(1);
      }, 2500); //3000 + Math.floor(Math.random() * 2800));
      setTimeout(() => {
        this.actionDance[i].reset().fadeIn(1).play();
        this.actionIntro[i].fadeOut(1);
      }, this.videoTimeins[this.VIDEONUM] * 1000 + Math.floor(Math.random() * 300));
    }
    //set different end dance times per video
    setTimeout(() => {
      this.songOver();
    }, this.videoTimeouts[this.VIDEONUM] * 1000);
  }
  cloneGltf = (gltf) => {
    const clone = {
      animations: gltf.animations,
      scene: gltf.scene.clone(true),
    };

    const skinnedMeshes = {};

    gltf.scene.traverse((node) => {
      if (node.isSkinnedMesh) {
        skinnedMeshes[node.name] = node;
      }
    });

    const cloneBones = {};
    const cloneSkinnedMeshes = {};

    clone.scene.traverse((node) => {
      if (node.isBone) {
        cloneBones[node.name] = node;
      }

      if (node.isSkinnedMesh) {
        cloneSkinnedMeshes[node.name] = node;
      }
    });

    for (let name in skinnedMeshes) {
      const skinnedMesh = skinnedMeshes[name];
      const skeleton = skinnedMesh.skeleton;
      const cloneSkinnedMesh = cloneSkinnedMeshes[name];

      const orderedCloneBones = [];

      for (let i = 0; i < skeleton.bones.length; ++i) {
        const cloneBone = cloneBones[skeleton.bones[i].name];
        orderedCloneBones.push(cloneBone);
      }

      cloneSkinnedMesh.bind(
        new THREE.Skeleton(orderedCloneBones, skeleton.boneInverses),
        cloneSkinnedMesh.matrixWorld
      );
    }

    return clone;
  };

  updatePlanePosition = () => {
    this.raycaster.setFromCamera(this.centerPoint, this.camera);
    const intersects = this.raycaster.intersectObject(this.surface);
    if (intersects.length === 1 && intersects[0].object === this.surface) {
      const pointX = intersects[0].point.x;
      const pointZ = intersects[0].point.z;

      this.lookAtMe.x = this.camera.position.x;
      this.lookAtMe.z = this.camera.position.z;
      // this.crosshairs.position.set(pointX, 0, pointZ);
      // this.crosshairs.lookAt(this.lookAtMe);
      // this.crosshairs.rotateX(-Math.PI / 2);
      this.view.position.set(pointX, 0, pointZ);
      this.view.lookAt(this.lookAtMe);
    }
  };

  update = () => {
    if (this.running) {
      if (!this.placed) this.updatePlanePosition();
      let tick = this.clock.getDelta();
      for (let i = 0; i < this.NUM_OF_FACES; i++)
        if (this.mixer[i] != null) this.mixer[i].update(tick);
      // get the average frequency of the sound
      // if (this.audioAnalyser) console.log(this.audioAnalyser.getAverageFrequency());
    }
  };
}
