import Logo from 'assets/svg/8thwall.svg'
import ErrorLogo from 'assets/img/error-icon.png'

import {isIOS, isMobileSafari, isChrome, isFirefox, isEdge} from 'react-device-detect';

export const customizeUI = () => {

  const loadImage = document.getElementById("loadImage")
  if (loadImage) {
    loadImage.src='';
    loadImage.style.displayy = 'none'
    loadImage.classList.remove('spin')
  }
  
  //powered by logo
  const images = document.querySelectorAll('.poweredby-img')
  for (let i = 0; i < images.length; i++) {
    images[i].src = Logo;
    images[i].style.width = '130px'
  }

  //error
  const error = document.querySelector('#cameraSelectionWorldTrackingError .floater')
  error.src = ErrorLogo

}

export const customizePreview = () => {
  
  const videoMessage = document.querySelector('#previewContainer .top-bar')
  videoMessage.insertAdjacentHTML('beforeend', '<div id="videoMessage">Click the download button below and check your downloads.</div>');

  if(isIOS && isMobileSafari && !isChrome && !isFirefox && !isEdge) {
    const preview = document.querySelector('#previewContainer .top-bar')
    preview.insertAdjacentHTML('beforeend', '<div id="customMessage">Click download, view, and then long press the image to save to camera roll.</div>');
  }
}