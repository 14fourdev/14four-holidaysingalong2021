import * as THREE from "three";

class TouchHandler {
  constructor() {
    this.tapX = 0;
    this.tapY = 0;
    this.tapZ = 0;
    this.initialSpread = 0;
    this.touchStartAngle = 0;
    this.objectStartAngle = 0;
    this.placing = true;
    this.dragging = false;
    this.pinching = false;
  }

  init(
    scene,
    camera,
    surface,
    object,
    MINSCALE,
    MAXSCALE = 1000,
    SCALEMULTIPLIER = 10
  ) {
    this.scene = scene;
    this.camera = camera;
    this.surface = surface;
    this.object = object;
    this.initialScale = object.scale.x;
    this.MINSCALE = MINSCALE;
    this.MAXSCALE = MAXSCALE;
    this.SCALEMULTIPLIER = SCALEMULTIPLIER;
  }

  endGesture() {
    this.dragging = false;
    this.pinching = false;
  }

  handleCanvasTouchPlace(
    e,
    state,
    frontFacing = true,
    isHologram = false,
    forcePosition = null
  ) {
    let tapPosition = new THREE.Vector2();
    tapPosition.x = (e.touches[0].clientX / window.innerWidth) * 2 - 1;
    tapPosition.y = -(e.touches[0].clientY / window.innerHeight) * 2 + 1;
    this.object.visible = true;
    if (this.object.children[0]) this.object.children[0].visible = true;
    if (frontFacing) {
      //PLACING ON 3D GROUND
      if (forcePosition === null) {
        let raycaster = new THREE.Raycaster();
        raycaster.setFromCamera(tapPosition, this.camera);
        const intersects = raycaster.intersectObject(this.surface);
        if (intersects.length === 1 && intersects[0].object === this.surface) {
          this.object.position.x = intersects[0].point.x;
          this.object.position.z = intersects[0].point.z;
        }
      } else {
        this.object.position.x = forcePosition.x;
        this.object.position.z = forcePosition.z;
      }
      //object face camera
      this.object.rotation.y = Math.atan2(
        this.camera.position.x - this.object.position.x,
        this.camera.position.z - this.object.position.z
      );
    } //PlACING IN 2D
    else {
      let raycaster = new THREE.Raycaster();
      raycaster.setFromCamera(tapPosition, this.camera);
      const intersects = raycaster.intersectObject(this.surface);
      if (intersects.length === 1 && intersects[0].object === this.surface) {
        let spot = this.surface.worldToLocal(intersects[0].point);
        if (state === "start") {
          this.tapX = spot.x;
          this.tapY = spot.y;
          this.tapZ = spot.z;
          this.pinching = false;
          this.dragging = true;
        }
        if (state === "move" && this.dragging) {
          let deltaX = this.tapX - spot.x;
          let deltaY = this.tapY - spot.y;
          let deltaZ = this.tapZ - spot.z;
          this.tapX = spot.x;
          this.tapY = spot.y;
          this.tapZ = spot.z;
          if (deltaX || deltaY || deltaZ) {
            this.object.position.x -= deltaX;
            this.object.position.y -= deltaY;
            this.object.position.z -= deltaZ;
          }
        }
      }
    }
  }

  handleCanvasTouchScale(e, state) {
    const touchList = [];
    for (let i = 0; i < 2; i++) {
      touchList.push(e.touches[i]);
    }
    let ts = {
      touchCount: touchList.length,
    };
    const centerPositionRawX =
      touchList.reduce((sum, touch) => sum + touch.clientX, 0) /
      touchList.length;
    const centerPositionRawY =
      touchList.reduce((sum, touch) => sum + touch.clientY, 0) /
      touchList.length;
    ts.positionRaw = { x: centerPositionRawX, y: centerPositionRawY };
    const screenScale = 2 / (window.innerWidth + window.innerHeight);
    ts.position = {
      x: centerPositionRawX * screenScale,
      y: centerPositionRawY * screenScale,
    };
    let spread =
      touchList.reduce(
        (sum, touch) =>
          sum +
          Math.sqrt(
            Math.pow(centerPositionRawX - touch.clientX, 2) +
              Math.pow(centerPositionRawY - touch.clientY, 2)
          ),
        0
      ) / touchList.length;
    spread = spread * screenScale * this.SCALEMULTIPLIER;

    if (state === "start") {
      this.initialSpread = spread;
      this.initialScale = this.object.scale.x;
      this.dragging = false;
      this.pinching = true;
    }
    if (state === "move" && this.pinching) {
      let currentScale = this.initialScale + (spread - this.initialSpread);
      currentScale = Math.min(
        Math.max(currentScale, this.MINSCALE),
        this.MAXSCALE
      );
      this.object.scale.set(currentScale, currentScale, currentScale);
      // console.log("SCALE:" + currentScale);
    }
  }

  handleCanvasTouchRotation(e, state) {
    var dx = e.touches[1].clientX - e.touches[0].clientX;
    var dy = e.touches[1].clientY - e.touches[0].clientY;
    if (state === "start") {
      this.touchStartAngle = Math.atan2(dy, dx);
      this.objectStartAngle = this.object.rotation.z;
      this.dragging = false;
      this.pinching = true;
    }
    if (state === "move" && this.pinching) {
      var angleChange = Math.atan2(dy, dx) - this.touchStartAngle;
      let rot = this.objectStartAngle - angleChange;
      // if (rot > 1) rot = 1;
      // if (rot < -1) rot = -1;
      this.object.rotation.z = rot;
    }
  }
}

export default new TouchHandler();
