import * as BABYLON from "babylonjs";
// import "@babylonjs/loaders/glTF";
import "babylonjs-loaders";

export default class AssetLoader {
  static loadCache = {
    models: {},
    textures: {},
  };
  static objectCache = {
    models: {},
    textures: {},
  };

  static loadModels = (models, scene) =>
    Promise.all(
      Object.keys(models).map((key) => this.loadModel(key, models[key], scene))
    );

  static loadModel = (key, path) =>
    (AssetLoader.loadCache.models[key] =
      AssetLoader.loadCache.models[key] ||
      new Promise((resolve) => {
        console.log(`Loading model ${key}: ${path}`);
        BABYLON.SceneLoader.ImportMesh(
          "",
          path,
          "",
          this.scene,
          function (meshes, particleSystems, skeletons, animationGroups) {
            resolve(
              (AssetLoader.objectCache.models[key] = {
                meshes,
                particleSystems,
                skeletons,
                animationGroups,
              })
            );
          }
        );
      }));

  static getModel = (key) => AssetLoader.objectCache.models[key];
}
