export default class ExperienceLoader {
  static cache = {}
  static load = experience => 
    ExperienceLoader.cache[experience] = ExperienceLoader.cache[experience] || new Promise(resolve => import(`../experiences/${experience}/index.js`).then(resolve))
}