import { RGBELoader } from "three/examples/jsm/loaders/RGBELoader.js";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
// import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'


// import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader";

import * as THREE from "three";

const gltfLoader = new GLTFLoader();
// const fbxLoader = new FBXLoader()
// const dracoLoader = new DRACOLoader();
// dracoLoader.setDecoderPath("/dracoDecoder/");
// gltfLoader.setDRACOLoader(dracoLoader);

export default class AssetLoader {
  static loadCache = {
    models: {},
    textures: {},
    hdris: {},
    sounds: {},
  };
  static objectCache = {
    models: {},
    textures: {},
    hdris: {},
    sounds: {},
  };
  static load = (assets) => {
    let arr = [];
    if (assets.models) {
      arr.push(AssetLoader.loadModels(assets.models));
    }
    if (assets.textures) {
      arr.push(AssetLoader.loadTextures(assets.textures));
    }
    if (assets.hdris) {
      arr.push(AssetLoader.loadHDRIS(assets.hdris));
    }
    if (assets.sounds) {
      arr.push(AssetLoader.loadSounds(assets.sounds));
    }
    return Promise.all(arr);
  };

  static loadModels = (models) =>
    Promise.all(
      Object.keys(models).map((key) => this.loadModel(key, models[key]))
    );
  static loadModel = (key, path) =>
    (AssetLoader.loadCache.models[key] =
      AssetLoader.loadCache.models[key] ||
      new Promise((resolve) => {
        console.log(`Loading model ${key}: ${path}`);
        if (path.endsWith(".glb") || path.endsWith(".gltf")) {
          gltfLoader.load(path, (gltf) =>
            resolve((AssetLoader.objectCache.models[key] = gltf))
          );
        }
        // if (path.endsWith('.fbx')) {
        //   fbxLoader.load(path, gltf => resolve(AssetLoader.objectCache.models[key] = gltf))
        // }
      }));
  static getModel = (key) => AssetLoader.objectCache.models[key];

  static loadTextures = (textures) =>
    Promise.all(
      Object.keys(textures).map((key) => this.loadTexture(key, textures[key]))
    );
  static loadTexture = (key, path) =>
    (AssetLoader.loadCache.textures[key] =
      AssetLoader.loadCache.textures[key] ||
      new Promise((resolve) => {
        console.log(`Loading texture ${key}: ${path}`);
        let textureloader = new THREE.TextureLoader();
        textureloader.load(
          path,
          (texture) => {
            resolve((AssetLoader.objectCache.textures[key] = texture));
          },
          false
        );
      }));

  static getTexture = (key) => AssetLoader.objectCache.textures[key];

  static loadHDRIS = (hdris) =>
    Promise.all(
      Object.keys(hdris).map((key) => this.loadHDRI(key, hdris[key]))
    );
  static loadHDRI = (key, path) =>
    (AssetLoader.loadCache.hdris[key] =
      AssetLoader.loadCache.hdris[key] ||
      new Promise((resolve) => {
        console.log(`Loading hdri ${key}: ${path}`);
        let hdriloader = new RGBELoader();
        hdriloader.setDataType(THREE.UnsignedByteType);
        hdriloader.load(
          path,
          (hdri) => {
            resolve((AssetLoader.objectCache.hdris[key] = hdri));
          },
          false
        );
      }));

  static getHDRI = (key) => AssetLoader.objectCache.hdris[key];

  static loadSounds = (sounds) =>
    Promise.all(
      Object.keys(sounds).map((key) => this.loadSound(key, sounds[key]))
    );
  static loadSound = (key, path) =>
    (AssetLoader.loadCache.sounds[key] =
      AssetLoader.loadCache.sounds[key] ||
      new Promise((resolve) => {
        console.log(`Loading sound ${key}: ${path}`);
        let soundloader = new THREE.AudioLoader();
        soundloader.load(
          path,
          (sound) => {
            resolve((AssetLoader.objectCache.sounds[key] = sound));
          }
        );
      }));

  static getSound = (key) => AssetLoader.objectCache.sounds[key];
}
