import React, { useEffect, useState } from 'react'
import s from './Main.module.scss'
import VerticleHeight from 'components/VerticleHeight';
import Landscape from 'components/Landscape';
import { isMobile } from 'react-device-detect';
import DesktopPage from 'components/Desktop/Desktop';

const Main = ({children}) => {
  const [isLandscape, setLandscape] = useState(window.screen.width > window.screen.height);
  const [showDesktopPage] = useState(!isMobile);

  useEffect(() => {
    window.addEventListener('orientationchange', () => {
      if(window.screen.width > window.screen.height) {
        setLandscape(true);
      } else {
        setLandscape(false);
      }
    });
  });

  return (
    <div className={s.Main}>
      <VerticleHeight className={s.verticleHeight}>
        {showDesktopPage && <DesktopPage />}
        {isLandscape && !showDesktopPage && <Landscape/>}
        {!isLandscape && children}
      </VerticleHeight>
    </div>
  )

}

export default Main
